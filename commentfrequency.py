import praw
import codecs
import json
with codecs.open("/home/pi/credentials/redditConfig.json", 'r',encoding='utf8') as f:
    data = json.load(f)

reddit = praw.Reddit(client_id=data["client_id"],
                     client_secret=data["client_secret"],
                     user_agent=data["user_agent"],
                     username=data["username"],
                     password=data["password"])

subreddit = reddit.subreddit('norge')
for comment in subreddit.stream.comments(skip_existing=true):
    print(comment)