import sys
import json
import discord
import codecs
import praw
from discord.ext import commands
sys.dont_write_bytecode = True

with codecs.open("/home/pi/credentials/discordConfig.json", 'r',encoding='utf8') as f:
    data = json.load(f)
    token = data["token"]
    prefix = data["prefix"]
    channelID = data["livefeed"]

with codecs.open("/home/pi/credentials/redditConfig.json", 'r',encoding='utf8') as f:
    data = json.load(f)
    reddit = praw.Reddit(client_id=data["client_id"],
                     client_secret=data["client_secret"],
                     user_agent=data["user_agent"],
                     username=data["username"],
                     password=data["password"])
    sub = data["sub"]
embedColour = 0x50bdfe
bot = commands.Bot(command_prefix=prefix, prefix=prefix)
@bot.event
async def on_ready():
    for guild in bot.guilds:
        print(f'Logged in as: {bot.user.name} in {guild.name}. Version: {discord.__version__}')
    channel = guild.get_channel(channelID)
    subreddit = reddit.subreddit(sub)
    for comment in subreddit.stream.comments(skip_existing=True):
        desc = comment.body
        embed = discord.Embed(description=desc, color=embedColour)
        embed.set_author(name=comment.author,url="https://www.reddit.com/u/"+str(comment.author))
        embed.add_field(name="Links", value="[permalink](https://old.reddit.com" + comment.permalink + ") [context](https://old.reddit.com" + comment.permalink + "?context=1000)")
        await channel.send(embed=embed)
    
print("Starting bot...")
bot.run(token, bot=True, reconnect=True)
